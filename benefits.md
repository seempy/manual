# Beneficios

Hola! Aquí la lista de todos los beneficios de trabajar en Seempy. Muchos de los beneficios no aplican para otros paises que no sean Costa Rica. Debido que la mayoria del equipo se encuentra operando en este país, los beneficios de salud aplican para Costa Rica por el momento


## Salud y Seguro Social

Seempy opera bajo las leyes de Costa Rica, por lo tanto, cada colaborador residente en Costa Rica tiene derecho por ley al seguro social. 

## Vacaciones

Seempy te otorga 3 semanas de vacaciones pagadas al año. Puedes utilizarlas como desees. Acumulas 1.25 dias de vacaciones por mes. Esto quiere decir que desde el primer mes empiezas a acumular dias de vacaciones y las puedes utilizar en el momento que desees. 

Como puedes utilizar los dias a tu antojo, si es necesario coordinar con el equipo para que todas tus tareas queden cubiertas. 

En caso que te enfermes, no hay problema, vuelve tan pronto te sientas mejor. En caso que tome mas de 3 días laborales, debes conversar con Sergio. En caso, que te encuentres sobrecargado mentalmente, tambien puedes tomar un día para recargar energias, la salud mental es igual de valiosa que la salud fisica. 

Si llevas 3 años o mas en Seempy. Tienes derecho a un mes completo de vacaciones por año. Igualmente a distribuirse como lo desees. 

Cada 5 años, los colaboradores tienen el derecho a tomarse un mes total de vacaciones totalmente pagados por Seempy. Unicamente hay que coordinar con el equipo, preferiblemente con 4 meses de antelación. 

Además de todos los feriados por ley en el país que te encuentres.

## Trabajo remoto

No es de nuestro interes el lugar donde trabajas. Creemos en el trabajo remoto y global. El talento no se concentra en una sola zona. Es por eso, que puedes trabajar desde cualquier parte. 

Toma en cuenta en caso que trabajes desde lugares publicos, que esto no afecte tu concentración. De igual forma Seempy tiene oficinas en San Jose, Costa Rica, donde estas invitado a laborar.

## Especialista en nutrición y estrés

En Seempy sabemos que como seres integrales debemos de cuidar nuestra salud y uno de los principales pilares de esto es la alimentación. Por esto contamos con Fran (o Francesca), nutricionista especializada en temas relacionados con estrés y salud mental quien está 100% disponible y anuente para trabajar con cada uno de nuestros colaboradores. ¡Solamente deben de contactarse con ella!

